#include "triangulo.hpp"
#include <iostream>

Triangulo:Triangulo() {
    tipo = "Triângulo";
    base = 1.0f;
    altura = 1.0f;
}
Triangulo::Triangulo(float base, float altura){
    tipo = "Triângulo";
    set_base(base);
    set_altura(altura);
}
Triangulo::Triangulo(){
}

float Triangulo::calculoArea(){
    return (base*altura/2);
}
float Triangulo::calculoPerimetro(){
    return 3*base;
}
