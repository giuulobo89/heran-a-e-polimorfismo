#include "hexagono.hpp"
#include <iostream>
#include <math.h>

Hexagono::Hexagono(){
	tipo = "Hexagono";
	base = 1.0f;
}

Hexagono::Hexagono(float base){
	tipo = "Hexagono";
	set_base(base);
}

Hexagono::~Hexagono(){}

float Hexagono::calculoArea(){
	return (3*base*base*(sqrt(3)))/2;
}

float Hexagono::calculoPerimetro(){
	return 6*base;
}
