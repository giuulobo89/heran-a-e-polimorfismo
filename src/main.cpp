#include "quadrado.hpp"
#include "triangulo.hpp"
#include "circulo.hpp"
#include "hexagono.hpp"
#include "pentagono.hpp"
#include "paralelogramo.hpp"
#include "formageometrica.hpp"
#include <iostream>
#include <string>
#include <vector.h>

using namespace std;

int main(int argc, char ** argv){
    vector<FormaGeometrica*>formasGeometricas;

    Quadrado * quadrado1 = new Quadrado(3.0f);
    formasGeometricas.push_back(quadrado1);
    
    Triangulo * triangulo1 = new Triangulo(3.0f, 3.0f);
    formasGeometricas.push_back(triangulo1);
    
    Circulo * circulo1 = new Circulo(3.0f);
    formasGeometricas.push_back(circulo1);
    
    Hexagono * hexagono1 = new Hexagono(3.0f, 3.0f);
    formasGeometricas.push_back(hexagono1);
    
    Pentagono * pentagono1 = new Pentagono(3.0f, 3.0f);
    formasGeometricas.push_back(pentagono1);
    
    Paralelogramo * paralelogramo1 = new Paralelogramo(3.0f, 3.0f);
    formasGeometricas.push_back(paralelogramo1);
    
    for(auto x:formasGeometricas){
        cout << x.get_tipo() << endl
        cout << x.calculoArea() << endl
        cout << x.calculoPerimetro() << endl
    }
    

return 0;
}

