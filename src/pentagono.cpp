#include "pentagono.hpp"
#include <iostream>

Pentagono::Pentagono(){
	tipo = "Pentagono";
	base = 1.0f;
	altura = 1.0f;
}

Pentagono::Pentagono(float base, float altura){
	tipo = "Pentagono";
	set_base(base);
	set_altura(altura);
}

Pentagono::~Pentagono(){}

float Pentagono::calculoArea(){
	return (5*base*altura)/2;
}

float Pentagono::calculoPerimetro(){
	return 5*base;
}
