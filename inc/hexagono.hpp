#ifndef HEXAGONO.HPP
#define HEXAGONO.HPP

#include "formageometrica.hpp"

class Hexagono : public FormaGeometrica{

    public:
    Hexagono();
    Hexagono(float base);
    ~Hexagono();
    
    float calculoArea();
    float calculoPerimetro();
    
};

#endif
