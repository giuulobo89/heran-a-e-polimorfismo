#ifndef PENTAGONO.HPP
#define PENTAGONO.HPP

#include "formageometrica.hpp"

class Pentagono : FormaGeometrica{

    public:
    Pentagono();
    Pentagono(float base, float altura);
    ~Pentagono();
    
    float calculoArea();
    float calculoPerimetro();
    
};

#endif
