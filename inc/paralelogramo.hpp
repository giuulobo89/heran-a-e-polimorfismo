#ifndef PARALELOGRAMO.HPP
#define PARALELOGRAMO.HPP

#include "formageometrica.hpp"

class Paralelogramo : public FormaGeometrica{

    public:
    Paralelogramo();
    Paralelogramo(float base, float altura);
    ~Paralelogramo();
    
};

#endif
