#ifndef CIRCULO.HPP
#define CIRCULO.HPP

#include "formulageometrica.hpp"

class Circulo : public FormaGeometrica(){

    private:
        float raio;

    public:
        Circulo();
        Circulo(float raio);
        ~Circulo();
        
        float calculoArea();
        float calculoPerimetro();
        
};

#endif
